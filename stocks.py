#!/usr/bin/env python
from click import command, option
from requests import get

ALPHAVANTAGE_BASE_URL = "https://www.alphavantage.co/query"
CLOSED = "4. close"
ONE_MINUTE_TIME_SERIES = "Time Series (1min)"


def get_stock_data(api_token, symbol):
    params = {
        "function": "TIME_SERIES_INTRADAY",
        "symbol": symbol,
        "interval": "1min",
        "apikey": api_token,
    }
    response = get(ALPHAVANTAGE_BASE_URL, params=params)
    return response.json()


def get_latest_price(data):
    latest_values = data[ONE_MINUTE_TIME_SERIES]
    timestamps = list(latest_values.keys())
    latest_time = timestamps[0]
    last_value = latest_values[latest_time]
    return latest_time, last_value.get(CLOSED, "")


@command()
@option(
    "--symbol",
    help="Stock symbol for which to retrieve data",
)
@option(
    "--api-token",
    help="API Token for Alpha Vantage",
    envvar="ALPHAVANTAGE_TOKEN",
)
def main(api_token, symbol):
    data = get_stock_data(api_token, symbol)
    latest_price = get_latest_price(data)
    output_string = f"Price for {symbol} as of {latest_price[0]}: {latest_price[1]}" # noqa
    print(output_string)


main()
